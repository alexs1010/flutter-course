# Excercise: Components in Flutter

This is the work in progress practice about components in flutter, it consume local json file to fill a ListView, it use Future, FutureBuilder, jsonDecode, simple example of provider, setting up routes, navigator and basic widgets used.

![Components example](https://gitlab.com/alexs1010/flutter-course/-/raw/main/flutter_components/exampleComponents.png)

By. Alex S
