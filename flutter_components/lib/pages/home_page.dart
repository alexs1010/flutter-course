import 'package:flutter/material.dart';
import 'package:flutter_components/providers/menu_provider.dart';
import 'package:flutter_components/utils/icons.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Componentes Flutter"),
      ),
      drawer: Container(
        width: 300,
        color: Colors.blue,
      ),
      body: _list(),
    );
  }

  Widget _list() {
    return FutureBuilder(
      future: menuProvider.loadData(),
      initialData: [],
      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
        return ListView(
          children: _listItem(snapshot.data, context),
        );
      },
    );
  }

  List<Widget> _listItem(List<dynamic> data, BuildContext context) {
    List<Widget> options = [];
    data.forEach((item) {
      options.add(Column(
        children: [
          ListTile(
            onTap: () {
              Navigator.pushNamed(context, item["ruta"]);
            },
            title: Text(item["texto"]),
            leading: getIcon(item["icon"]),
            trailing: Icon(Icons.arrow_forward_ios, color: Colors.blue),
          ),
          Divider(),
        ],
      ));
    });
    return options;
  }
}
