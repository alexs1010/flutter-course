import 'package:flutter/material.dart';

class CardPage extends StatelessWidget {
  const CardPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Colors.orange,
      appBar: AppBar(
        title: Text("Cards Example"),
      ),
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
        child: ListView(
          children: [
            _makeCardType1(),
            SizedBox(height: 20.0),
            _makeCardType2(
                "https://i.pinimg.com/originals/a7/32/10/a7321091385d01b17f363f8e734e898c.jpg"),
            SizedBox(height: 20.0),
            _makeCardType2(
                "https://images.ctfassets.net/r7p9m4b1iqbp/6VQakelQsfweSmtfqyGVkR/e1a5ae9c3efd46b992c8e860082c2010/Pocono-Cabins-Little-AFrame-1.jpg?w=600&fm=&fm=jpg&fl=progressive&q=90"),
            SizedBox(height: 20.0),
            _makeCardType1(),
            SizedBox(height: 20.0),
            _makeCardType2(
                "https://i.pinimg.com/originals/86/3f/fa/863ffa7535d2a3c231c534deb97e5b53.jpg"),
          ],
        ),
      ),
    );
  }

  Widget _makeCardType1() {
    return Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
        children: [
          ListTile(
            title: Text("Header of my card"),
            subtitle: Text(
                "Subttle of my card with lipsum lorum etc ble ble ble transilvanian language"),
            leading: Icon(Icons.photo_album, color: Colors.green),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              TextButton(
                onPressed: null,
                child: Text("Action one"),
              ),
              TextButton(
                onPressed: null,
                child: Text("Action two"),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _makeCardType2(String image) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20.0),
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 5.0,
            spreadRadius: 0.0,
            offset: Offset(2.0, 7.0),
          ),
        ],
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20.0),
        child: Container(
          color: Colors.white,
          child: Column(
            children: [
              FadeInImage(
                image: NetworkImage(image),
                placeholder:
                    AssetImage('assets/images/placeholder-loading.gif'),
                fadeInDuration: Duration(milliseconds: 300),
                fit: BoxFit.cover,
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Text(
                    "House of fild with nice natural views, with wood and awesome and beautifil views"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
