import 'package:flutter/material.dart';

class HomeTempPage extends StatelessWidget {
  const HomeTempPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
          appBar: AppBar(title: Text("Componentes")),
          body: ListView(
            children: [
              ListTile(
                onLongPress: () {
                  print("click mantenido");
                },
                onTap: () {
                  print("Entrando el click");
                },
                title: Text("Nombresito"),
                subtitle: Text("Tipo"),
                leading: CircleAvatar(),
                trailing: Icon(Icons.arrow_forward_ios),
              ),
              ListTile(
                onTap: () {
                  print("Entrando el click");
                },
                title: Text("Nombresito"),
                subtitle: Text("Tipo"),
                leading: CircleAvatar(),
                trailing: Icon(Icons.arrow_forward_ios),
              ),
              ListTile(
                onTap: () {
                  print("Entrando el click");
                },
                title: Text("Nombresito"),
                subtitle: Text("Tipo"),
                leading: CircleAvatar(),
                trailing: Icon(Icons.arrow_forward_ios),
              ),
              ListTile(
                onTap: () {
                  print("Entrando el click");
                },
                title: Text("Nombresito"),
                subtitle: Text("Tipo"),
                leading: CircleAvatar(),
                trailing: Icon(Icons.arrow_forward_ios),
              ),
              ListTile(
                onTap: () {
                  print("Entrando el click");
                },
                title: Text("Nombresito"),
                subtitle: Text("Tipo"),
                leading: CircleAvatar(),
                trailing: Icon(Icons.arrow_forward_ios),
              ),
              ListTile(
                onTap: () {
                  print("Entrando el click");
                },
                title: Text("Nombresito"),
                subtitle: Text("Tipo"),
                leading: CircleAvatar(),
                trailing: Icon(Icons.arrow_forward_ios),
              ),
              ListTile(
                onTap: () {
                  print("Entrando el click");
                },
                title: Text("Nombresito"),
                subtitle: Text("Tipo"),
                leading: CircleAvatar(),
                trailing: Icon(Icons.arrow_forward_ios),
              ),
              ListTile(
                onTap: () {
                  print("Entrando el click");
                },
                title: Text("Nombresito"),
                subtitle: Text("Tipo"),
                leading: CircleAvatar(),
                trailing: Icon(Icons.arrow_forward_ios),
              ),
              ListTile(
                onTap: () {
                  print("Entrando el click");
                },
                title: Text("Nombresito"),
                subtitle: Text("Tipo"),
                leading: CircleAvatar(),
                trailing: Icon(Icons.arrow_forward_ios),
              ),
              ListTile(
                onTap: () {
                  print("Entrando el click");
                },
                title: Text("Nombresito"),
                subtitle: Text("Tipo"),
                leading: CircleAvatar(),
                trailing: Icon(Icons.arrow_forward_ios),
              ),
              ListTile(
                onTap: () {
                  print("Entrando el click");
                },
                title: Text("Nombresito"),
                subtitle: Text("Tipo"),
                leading: CircleAvatar(),
                trailing: Icon(Icons.arrow_forward_ios),
              ),
              ListTile(
                onTap: () {
                  print("Entrando el click");
                },
                title: Text("Nombresito"),
                subtitle: Text("Tipo"),
                leading: CircleAvatar(),
                trailing: Icon(Icons.arrow_forward_ios),
              ),
              ListTile(
                onTap: () {
                  print("Entrando el click");
                },
                title: Text("Nombresito"),
                subtitle: Text("Tipo"),
                leading: CircleAvatar(),
                trailing: Icon(Icons.arrow_forward_ios),
              ),
              ListTile(
                onTap: () {
                  print("Entrando el click");
                },
                title: Text("Nombresito"),
                subtitle: Text("Tipo"),
                leading: CircleAvatar(),
                trailing: Icon(Icons.arrow_forward_ios),
              ),
            ],
          )),
    );
  }
}
