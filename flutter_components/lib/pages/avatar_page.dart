import 'package:flutter/material.dart';

class AvatarPage extends StatelessWidget {
  const AvatarPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text("Avatar example"),
        actions: [
          CircleAvatar(
            child: ClipOval(
              child: Image(
                image: NetworkImage(
                    "https://avatars.githubusercontent.com/u/6085?v=4"),
              ),
            ),
          ),
          CircleAvatar(
            backgroundImage: NetworkImage(
                "https://i.pinimg.com/280x280_RS/71/56/4a/71564aa3bfd7836242f619a577ee4264.jpg"),
          ),
          CircleAvatar(
            backgroundImage: AssetImage('assets/images/cat-face.jpg'),
          ),
          Container(
            width: 40.0,
            height: 40.0,
            decoration: BoxDecoration(
              color: Colors.blue,
              shape: BoxShape.circle,
              image: DecorationImage(
                image: NetworkImage(
                    "https://i.pinimg.com/150x150_RS/cc/a2/96/cca296473bf43bc1f98ef7d55396a420.jpg"),
              ),
            ),
          ),
        ],
      ),
      body: Center(
        child: ListView(
          children: [
            Image.asset(
              'assets/images/cat-face.jpg',
              height: 200,
            ),
            SizedBox(height: 40.0),
            Image(
              height: 200,
              image: AssetImage('assets/images/cat-face2.jpg'),
            ),
          ],
        ),
      ),
    );
  }
}
