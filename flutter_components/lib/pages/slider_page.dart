import 'dart:ui';

import 'package:flutter/material.dart';

class SliderPage extends StatefulWidget {
  const SliderPage({Key key}) : super(key: key);

  @override
  _SliderPageState createState() => _SliderPageState();
}

class _SliderPageState extends State<SliderPage> {
  double _valueSlider = 10.0;
  bool _istrue = false;
  bool _istrue2 = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        margin: EdgeInsets.symmetric(
          horizontal: 20.0,
          vertical: 15.0,
        ),
        child: Column(children: [
          _makeSlider(),
          _makeCheckBox(),
          _makeSwitch(),
          Expanded(
            child: _showImage(),
          ),
        ]),
      ),
    );
  }

  Widget _makeCheckBox() {
    return CheckboxListTile(
      title: Text("Activate"),
      value: _istrue,
      onChanged: (value) {
        _istrue = value;
        setState(() {});
      },
    );
  }

  Widget _showImage() {
    return Image(
        width: _valueSlider,
        image: AssetImage('assets/images/cat-face.jpg'),
        fit: BoxFit.contain);
  }

  Widget _makeSwitch() {
    return SwitchListTile(
      title: Text("Select or activate"),
      value: _istrue2,
      onChanged: (value) {
        _istrue2 = value;
        setState(() {});
      },
    );
  }

  Widget _makeSlider() {
    return Slider(
        label: "Size",
        value: _valueSlider,
        min: 10.0,
        max: 400.0,
        onChanged: (value) {
          _valueSlider = value;
          setState(() {});
        });
  }
}
