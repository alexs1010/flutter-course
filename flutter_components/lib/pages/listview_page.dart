import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';

class ListviewPage extends StatefulWidget {
  const ListviewPage({Key key}) : super(key: key);

  @override
  _ListviewPageState createState() => _ListviewPageState();
}

class _ListviewPageState extends State<ListviewPage> {
  List<int> _numberList = [];
  ScrollController _scrollController = new ScrollController();
  bool _isLoading = false;
  int _lastItem = 0;

  @override
  void initState() {
    _addItems();
    _scrollController.addListener(
      () {
        if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent) {
          fetchData();
        }
      },
    );
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("List View Example"),
      ),
      body: Stack(
        children: [
          _makeList(),
          _makeLoading(),
        ],
      ),
    );
  }

  Widget _makeList() {
    _addItems();

    return RefreshIndicator(
      child: ListView.builder(
          controller: _scrollController,
          itemCount: _numberList.length,
          itemBuilder: (context, index) {
            var rng = new Random();
            final image = rng.nextInt(50);
            return FadeInImage(
              placeholder: AssetImage("assets/images/placeholder-loading.gif"),
              image:
                  NetworkImage('https://picsum.photos/500/300/?image=$image'),
            );
          }),
      onRefresh: _getPage,
    );
  }

  Future<void> _getPage() {
    final _duration = Duration(milliseconds: 2);
    new Timer(_duration, () {
      _numberList.clear();
      _lastItem++;
      _addItems();
    });
    return Future.delayed(_duration);
  }

  void _addItems() {
    for (var i = 1; i < 10; i++) {
      _lastItem++;
      _numberList.add(_lastItem);
    }
  }

  Future<void> fetchData() async {
    _isLoading = true;
    setState(() {});
    return new Timer(Duration(seconds: 2), responseHttpSimulator);
  }

  void responseHttpSimulator() {
    _isLoading = false;
    setState(() {});
    _scrollController.animateTo(
      _scrollController.position.pixels + 200,
      duration: Duration(seconds: 300),
      curve: Curves.fastOutSlowIn,
    );

    _addItems();
  }

  Widget _makeLoading() {
    if (_isLoading) {
      return Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              CircularProgressIndicator(),
            ]),
          ]);
    } else {
      return Container();
    }
  }
}
