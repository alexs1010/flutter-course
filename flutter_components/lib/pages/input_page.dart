import 'package:flutter/material.dart';

class InputPage extends StatefulWidget {
  const InputPage({Key key}) : super(key: key);

  @override
  _InputPageState createState() {
    return _InputPageState();
  }
  // Same function but with arrowed style is
  // _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  String _name;
  String _email;
  String _date;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Inputs controls")),
      body: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: [
            _makeInput(),
            SizedBox(height: 20),
            Text("My name is $_name"),
            SizedBox(height: 20),
            _makeEmailInput(),
            SizedBox(height: 20),
            SizedBox(height: 20),
            _makePassword(),
            SizedBox(height: 20),
            _makeDateSelect(),
            SizedBox(height: 20),
            Text("The selected date is $_date"),
          ],
        ),
      ),
    );
  }

  Widget _makeInput() {
    return TextField(
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        hintText: "Input some Text",
        labelText: "Text",
        suffix: Icon(Icons.person),
        icon: Icon(Icons.account_circle),
      ),
      onChanged: (value) {
        _name = value;
        setState(() {});
      },
    );
  }

  Widget _makeEmailInput() {
    return TextField(
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          hintText: "Input your email",
          labelText: "Email",
          suffix: Icon(Icons.email),
          icon: Icon(Icons.email_outlined),
        ),
        onChanged: (value) {
          _email = value;
          setState(() {});
        });
  }

  Widget _makePassword() {
    return TextField(
      obscureText: true,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          hintText: "Input your Password",
          labelText: "Password",
          suffixIcon: Icon(Icons.vpn_key_sharp),
          icon: Icon(Icons.vpn_key_sharp)),
      onChanged: (value) {},
    );
  }

  Widget _makeDateSelect() {
    return TextField(
      obscureText: true,
      decoration: InputDecoration(
        hintText: "Select a date",
        labelText: "Date",
        suffixIcon: Icon(Icons.calendar_today),
        icon: Icon(Icons.calendar_today),
      ),
      onTap: () {
        _selectDate(context);
      },
    );
  }

  _selectDate(BuildContext context) async {
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: new DateTime.now(),
      firstDate: new DateTime(2018),
      lastDate: new DateTime(2025),
      locale: Locale("es", "ES"),
    );
    if (picked != null) {
      setState(() {
        _date = picked.toString();
      });
    }
  }
}
