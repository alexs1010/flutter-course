import 'package:flutter/material.dart';

class AlertPage extends StatelessWidget {
  const AlertPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Alert Screen"),
        ),
        body: Center(
            child: RaisedButton(
          onPressed: () {
            _showAlert(context);
          },
          child: Text("Show Alert"),
          color: Colors.blue,
          textColor: Colors.white,
          shape: StadiumBorder(),
        )));
  }

  void _showAlert(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              title: Text("Alert header Title"),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text("This is the content of my dialog widget alert dialog"),
                  FlutterLogo(size: 100.0),
                ],
              ),
              actions: [
                InkWell(
                  onTap: () => Navigator.of(context).pop(),
                  child: Text("Cancel With InkWell widget"),
                ),
                TextButton(
                    onPressed: () => Navigator.of(context).pop(),
                    child: Text("Cancel with TextButton")),
                FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("Cancel with FlatButton"),
                ),
                FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text("Ok with FlattButton"),
                )
              ]);
        });
  }
}
