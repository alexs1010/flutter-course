import 'package:flutter/material.dart';
import 'package:flutter_components/pages/alert_page.dart';
import 'package:flutter_components/pages/animation_container_page.dart';
import 'package:flutter_components/pages/avatar_page.dart';
import 'package:flutter_components/pages/card_page.dart';
import 'package:flutter_components/pages/home_page.dart';
import 'package:flutter_components/pages/input_page.dart';
import 'package:flutter_components/pages/listview_page.dart';
import 'package:flutter_components/pages/slider_page.dart';

Map<String, WidgetBuilder> getRoutes() {
  return <String, WidgetBuilder>{
    "/": (BuildContext context) => HomePage(),
    "alert": (BuildContext context) => AlertPage(),
    "avatar": (BuildContext context) => AvatarPage(),
    "card": (BuildContext context) => CardPage(),
    "inputs": (BuildContext context) => InputPage(),
    "slider": (BuildContext context) => SliderPage(),
    "animatedContainer": (BuildContext context) => AnimationContainerPage(),
    "list": (BuildContext context) => ListviewPage(),
  };
}
