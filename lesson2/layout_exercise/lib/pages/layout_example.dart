import 'package:flutter/widgets.dart';

import 'package:flutter/material.dart';

class LayoutExample extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;
    return Scaffold(
        appBar: AppBar(title: Text("My Layout Example App")),
        body: Column(
          children: [
            Image.asset(
                'assets/images/Saint_Mary_Lake_and_Wildgoose_Island.jpeg',
                fit: BoxFit.cover),
            Container(
              padding: EdgeInsets.all(32),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.only(bottom: 8),
                          child: Text(
                            "Saint Mary Lake and Wildgoose Island",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        Text(
                          "United States",
                          style: TextStyle(
                            color: Colors.grey[500],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Icon(Icons.star, color: Colors.red),
                  Text("41")
                ],
              ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.call, color: color),
                      Container(
                          margin: EdgeInsets.only(top: 8),
                          child: Text('CALL',
                              style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                color: color,
                              )))
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.near_me, color: color),
                      Container(
                          margin: EdgeInsets.only(top: 8),
                          child: Text('ROUTE',
                              style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                color: color,
                              )))
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.share, color: color),
                      Container(
                          margin: EdgeInsets.only(top: 8),
                          child: Text('SHARE',
                              style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                color: color,
                              )))
                    ],
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(32),
              child: Text(
                "Located on the east side of the park, Going-to-the-Sun Road parallels the lake along its north shore. At an altitude of 4,484 feet (1,367 m), Saint Mary Lake's waters are colder and lie almost 1,500 feet (460 m) higher in altitude than Lake McDonald, the largest lake in the park, which is located on the west side of the Continental Divide. Here, the great plains end and the Rocky Mountains begin in an abrupt 5,000-foot (1,500 m) altitude change, with Little Chief Mountain posing a formidable southern flank above the west end of the lake.",
                softWrap: true,
              ),
            )
          ],
        ));
  }
}
