# Excercise 2 - Layout practice

This is my versión of the [Layout practice](https://flutter.dev/docs/development/ui/layout/tutorial) challenge. It's just Layout without actions.

![My version of layout practice](https://gitlab.com/alexs1010/flutter-course/-/raw/main/lesson2/layout_exercise/finalWork.png)

By. Alex S
