/*
1.- Crear un programa con 3 variables enteros e identificar cual es el mayor, usar IF, ELSEIF, ELSE
2.- Crear una calculadora (suma, resta, multiplicación, división), usar funciones 
que reciba 2 parámetros e imprimir el resultado
3.- Recorrer los siguientes valores y mostrarlo en un print List<String> animals = ['Perro', 'Gato', 'Ratón']; 
usar forEach y función de flecha
4.- Combiar las siguientes listas y mostrarlo en un print 
List<String> animales = ['Conejo', 'Gato'];
List<String> mascotas = ['Tortuga', 'Perro'];
como resultado debe mostrar: [Conejo, Gato, Tortuga, Perro]
5.- Crear una función y como parámetro me permita recibir una lista de String (pintar los valores)
6.- Crear una función con argumentos con Nombre, como parámetros reciba una lista de String y otra lista de Enteros (pintar los valores)
7.- Crear una función que me retorne una Lista de String
8.- Crear una clase con 3 propiedades y mencionarlos en su constructor
 */

// excercise 01
void main() {
  int value1 = 5;
  int value2 = 70;
  int value3 = 60;

  if ((value1 > value2) && (value1 > value3))
    print(value1.toString() + " es mayor que " + value2.toString() + " y " + value3.toString());
  else if ((value2 > value1) && (value2 > value3))
    print(value2.toString() + " es mayor que " + value1.toString() + " y " + value3.toString());
  else
    print(value3.toString() + " es mayor que " + value2.toString() + " y " + value1.toString());
}

// excercise 02

void main() {
  calculator(10, "/", 20);
}

String calculator(double num1, String operation, double num2) {
  double result = 0.00;
  if (operation == "+") result = add(num1, num2);
  if (operation == "-") result = sub(num1, num2);
  if (operation == "*") result = mult(num1, num2);
  if (operation == "/") result = div(num1, num2);

  print(result.toString());
}

double add(double num1, double num2) {
  return num1 + num2;
}

double sub(double num1, double num2) {
  return num1 - num2;
}

double mult(double num1, double num2) {
  return num1 * num2;
}

double div(double num1, double num2) {
  return num1 / num2;
}

//excercise 03
void main() {
  List<String> animals = ['Perro', 'Gato', 'Ratón'];
  animals.forEach((animal) => print(animal));
}

//excercise 04
void main() {
  List<String> animales = ['Conejo', 'Gato'];
  List<String> mascotas = ['Tortuga', 'Perro'];

  animales.addAll(mascotas);
  animales.forEach((animal) => print(animal));
}

//excercise 05
//5.- Crear una función y como parámetro me permita recibir una lista de String (pintar los valores)

List<String> printList(List<String> values) {
  values.forEach((item) => print(item));
}

// excercise 06
// 6.- Crear una función con argumentos con Nombre, como parámetros reciba una lista de String y otra lista de Enteros (pintar los valores)
void printList(List<String> values, List<int> numbers) {
  values.forEach((item) => print(item));
  numbers.forEach((num) => print(num.toString()));
}

// excercise 07
List<String> returnListStrings() {
  List<String> values = ["Prueba", "Prueba", "Prueba"];
  return values;
}

// excercise 08
class Animal {
  String name;
  String age;
  String color;

  Animal(String name, String age, String color) {
    // There's a better way to do this, stay tuned.
    this.name = name;
    this.age = age;
    this.color = color;
  }
}
